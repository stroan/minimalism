color backgroundColor = #7FB25A;
color backgroundLine = #9EC680;
color textColor = backgroundColor * 10;

int screenWidth = 1000;
int screenHeight = 500;

PFont font;

// 0 = Main Screen
// 1 = Round Screen
// 2 = Game Screen
// 3 = Over Screen
int state;
int currentRound = 0;
int requiredMatches = 0;
int matchesThisRound = 0;
int score = 0;
long roundPrepTime;
long roundStartTime;
long roundTime;
long gameOverTime;

class Ball {
  public float size;
  public color col;
  public int x;
  public int y;
  
  public float destSize = 300;
  
  public Ball other;
  
  public Ball(color col, int x, int y) {
    this.col = col; 
    this.x = x;
    this.y = y;
  }
  
  public void reset() {
    size = 100;
    destSize = random(100, 350);
  }
  
  public boolean update(int currentVal, int maxVal) {
    size = 100 + (250 * ((float)currentVal / maxVal));
    return abs(size - destSize) < 18;
  }
  
  public void draw(boolean drawDest) {
    strokeWeight(10);
    stroke(backgroundColor);
    fill(col);
    ellipse(x, y, size, size);
    
    if (drawDest) {
      strokeWeight(3);
      stroke(other.col);
      noFill();
      ellipse(x, y, other.destSize, other.destSize);
    }
  }
}

Ball leftState = new Ball(#FF0044, 250, 250);
Ball rightState = new Ball(#FF4F00, 750, 250);

void setup() {
  size(1000, 500); 
  font = loadFont("Silkscreen-32.vlw");
  leftState.other = rightState;
  rightState.other = leftState;
  
  resetGame();
}

void draw() {
  update();
  
  drawBackground();
  
  if (state == 0) {
    drawInstructions();
  } else if (state == 1) {
    leftState.draw(false);
    rightState.draw(false);
    drawRoundScreen();
    drawScore();
  } else if (state == 2) {
    leftState.draw(true);
    rightState.draw(true);
    drawScore();
    drawTimer();
  } else if (state == 3) {
    leftState.draw(true);
    rightState.draw(true);
    drawScore();
    drawGameOver(); 
  }
}

void drawBackground() {
  background(backgroundColor);
  
  for (int i = -2; i < 50; i++) {
    int startX = (i * 30) + ((millis() / 100) % 30);
    int startY = 500;
    int endX = startX - 500;
    int endY = 0;
   
    stroke(backgroundLine);
    strokeWeight(10);
    line(startX, startY, endX, endY);
  }
  
  setupText();
  text("MINIMALISM", 500, 25);
}

void drawInstructions() {
  setupText();
  text("Left Click to Start.", 500, 200);
  text("Move Mouse to Play.", 500, 250);
  text("Match the circles to the same colored outlines.", 500, 300);
}

void drawRoundScreen() {
  setupText();
  text("Round " + currentRound, 500, 250);
  text("Match " + requiredMatches + " in " + (roundTime / 1000) + "s", 500, 275);
}

void drawScore() {
  setupText();
  text("Score: " + score, 500, 450); 
  text("Matched " + matchesThisRound + " of " + requiredMatches, 500, 475); 
}

void drawTimer() {
  setupText();
  text("" + remainingSeconds() + "s", 500, 250);
}

void drawGameOver() {
  setupText();
  text("Game Over", 500, 250); 
}

void setupText() {
  fill(textColor);
  textAlign(CENTER, CENTER);
  textFont(font, 32);
}

void update() {
  if (state == 1) {
    updateBalls();
    if (millis() - roundPrepTime > 3000) {
      startRound();
    }
  } else if (state == 2) {
    if (updateBalls()) {
      randomizeGoals();
      matchesThisRound++;
      
      if (requiredMatches == matchesThisRound) {
        score = score + (int)remainingSeconds();
        prepRound(currentRound + 1);
      }
    } else if (remainingSeconds() < 0) {
      gameOver();
    }
  } else if (state == 3) {
    if (millis() - gameOverTime > 5000) {
      resetGame();
    } 
  }
}

long remainingSeconds() {
  return floor(((roundTime - (millis() - roundStartTime)) / 1000));
}

void mouseClicked() {
  if (state == 0) {
    initGame();
  } else if (state == 1) {
    startRound();
  } else if (state == 3) {
    resetGame(); 
  }
}

boolean updateBalls() {
  boolean leftMatches = leftState.update(mouseX, screenWidth);
  boolean rightMatches = rightState.update(mouseY, screenHeight);
  return leftMatches && rightMatches;
}

void resetGame() {
  state = 0; 
}

void initGame() {
  score = 0;
  randomizeGoals();
  prepRound(1);
}

void prepRound(int r) {
  currentRound = r;
  matchesThisRound = 0;
  requiredMatches = currentRound;
  roundTime = 20000;
  roundPrepTime = millis();
  state = 1;
}

void startRound() {
  roundStartTime = millis();
  state = 2;
}

void randomizeGoals() {
  leftState.reset();
  rightState.reset(); 
}

void gameOver() {
  state = 3;
  gameOverTime = millis();
}
